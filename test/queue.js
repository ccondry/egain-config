const Egain = require('../src')
// create eGain interface object
const egain = new Egain({
  host: 'cceece.dcloud.cisco.com',
  username: 'sa',
  password: 'C1sco12345',
  db: 'eGActiveDB'
})

describe('queue.find()', function () {
  it('should find a Workflow > Queue', function (done) {
    egain.queue.find({
      queueName: 'chat',
      departmentName: '0325'
    })
    .then((results) => {
      console.log('found queue:', results)
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})

describe('queue.create()', function () {
  it('should create a Workflow > Queue', function (done) {
    egain.queue.create({
      queueId: 10325,
      callId: 'test',
      queueName: 'test',
      departmentId: 1000
    })
    .then(() => {
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})

describe('queue.setCallIdentifier()', function () {
  it('should set call identifier string for specified queue ID', function (done) {
    egain.queue.setCallIdentifier({
      queueId: 10325,
      callId: '0325'
    })
    .then(() => {
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})

describe('queue.list()', function () {
  it('should list Workflow > Queues', function (done) {
    egain.queue.list()
    .then(queues => {
      // const filteredQueues = queues.filter(v => {
      //   return v.queue_id === 10325
      // })
      // console.log(JSON.stringify(filteredQueues, null, 2))
      console.log('found', queues.length, 'queues')
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})

describe('queue.remove()', function () {
  it('should remove a Workflow > Queues', function (done) {
    egain.queue.remove(10325)
    .then(() => {
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})
