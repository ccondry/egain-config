const Egain = require('../../')
// create eGain interface object
const egain = new Egain({
  host: 'cceece.dcloud.cisco.com',
  username: 'sa',
  password: 'C1sco12345',
  db: 'eGActiveDB'
})

// run
go(1034).catch(e => console.log(e))

async function getDepartmentId (name) {
  try {
    const department = await egain.department.find({name})
    if (department) {
      return department.department_id
    } else {
      throw Error(`No eGain department found matching name "${name}"`)
    }
  } catch (e) {
    throw e
  }
}

async function go (queueId) {
  // set eGain queue to deleted state
  try {
    await egain.queue.delete({
      queueId: queueId
    })
    console.log(`successfully deleted eGain chat queue ${queueId}.`)
  } catch (e) {
    console.log('failed to delete eGain queue', queueId, e.message)
  }
  // remove ICM queue association
  try {
    await egain.icmQueue.delete({
      queueId: queueId
    })
    console.log(`successfully deleted ICM queue association for eGain chat queue ${queueId}.`)
  } catch (e) {
    console.log(`failed to delete ICM queue association eGain chat queue ${queueId}:`, e.message)
  }
  try {
    // find the existing department ID
    const departmentId = await getDepartmentId('test1')
    console.log(`successfully found department ID ${departmentId}.`)
    // find associated ACL
    const acl = await egain.acl.find({
      resourceId: queueId,
      resourceType: 1056, // 1056 = routing_queue type
      groupResource: 0, // this is a single resource, not a group resource
      departmentId
    })
    if (acl) {
      const aclId = acl.acl_id
      console.log(`successfully found ACL associated with eGain chat queue ${queueId}: ${aclId}.`)
      // remove associated ACL Permission
      try {
        await egain.aclPermission.delete({
          aclId: queueId
        })
        console.log(`successfully deleted ACL permission associated with chat queue ${queueId}.`)
      } catch (e) {
        console.log(`failed to delete ACL Permission associated with chat queue ${queueId}:`, e.message)
      }
      // remove associated ACL
      try {
        await egain.acl.delete({
          aclId: queueId
        })
        console.log(`successfully deleted ACL associated with chat queue ${queueId}.`)
      } catch (e) {
        console.log(`failed to delete ACL permission associated with chat queue ${queueId}:`, e.message)
      }
    } else {
      console.log(`did not find any ACL associated with chat queue ${queueId}.`)
    }
  } catch (e) {
    // failed to find department or ACL
    console.log('failed to find ACL or department ID:', e.message)
  }
  console.log('deprovision finished.')
}
