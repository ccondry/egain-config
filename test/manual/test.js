const Egain = require('../../')
// create eGain interface object
const egain = new Egain({
  host: 'cceece.dcloud.cisco.com',
  username: 'sa',
  password: 'C1sco12345',
  db: 'eGActiveDB'
})

// user ID
const user = {
  id: '0003'
}
// constants that you should get from ICM, or maybe the EGICM_QUEUE table
const chatMrdId = 5004
const chatScriptSelectorId = 5065
const emailMrdId = 5005
const emailScriptSelectorId = 5066
const emailRetrieverInstanceId = 999
// the target department name
const departmentName = user.id

//
const chatQueueId = 1059

// run
main().catch(e => console.log(e.message))

async function main () {
  try {
    await associateIcmScriptSelector({
      queueId: chatQueueId,
      mrdId: chatMrdId,
      scriptSelectorId: chatScriptSelectorId
      // maxTaskLimit: 5000
    })
  } catch (e) {
    throw e
  }
}

async function associateIcmScriptSelector ({queueId, mrdId, scriptSelectorId, maxTaskLimit = 5000}) {
  // try to find icm queue association
  console.log(`Searching for ICM queue association for queue ID ${queueId}...`)
  let row = await egain.icmQueue.find({queueId})
  if (row) {
    // queue association exists
    console.log(`Found eGain ICM queue association for queue ID ${queueId}: it uses ICM script selector ${scriptSelectorId} in MRD ${mrdId}.`)
  } else {
    // queue association does not exist
    console.log(`eGain ICM queue association for queue ID ${queueId} not found. Creating it...`, {queueId, mrdId, scriptSelectorId, maxTaskLimit})
    await egain.icmQueue.create({
      queueId,
      mrdId,
      scriptSelectorId,
      maxTaskLimit
    })
    console.log(`eGain ICM queue association for queue ID ${queueId} created.`)
  }
  console.log('associateIcmScriptSelector finished.')
}
