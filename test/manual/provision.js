const Egain = require('../../')
// create eGain interface object
const egain = new Egain({
  host: 'cceece.dcloud.cisco.com',
  username: 'sa',
  password: 'C1sco12345',
  db: 'eGActiveDB'
})

// user ID
const user = {
  id: '1001'
}

// run main function
main({
  departmentName: user.id,
  chatMrdId: 5004,
  emailMrdId: 5005,
  chatScriptSelectorId: 5065,
  emailScriptSelectorId: 5066,
  emailRetrieverInstanceId: 999, // rx-instance ID from eGMasterDB.dbo.EGPL_DSM_INSTANCE
  emailAddress: 'support_' + user.id + '@dcloud.cisco.com',
  pop3Server: 'branding.dcloud.cisco.com',
  pop3Port: 110,
  pop3Password: '3736363436353338363435353638343135343636363136363434373734333634333037413442353934443444343136363642373235303435353037373531353137353732364134333242363236383441373332463439334432333532343537363433333134353438333536353641354137343531353133443344',
  pop3LoginId: 'support_' + user.id,
  smtpPort: 25,
  smtpServer: 'branding.dcloud.cisco.com'
})
.catch(e => console.log(e))

// main function
async function main ({
  departmentName,
  chatMrdId,
  emailMrdId,
  chatScriptSelectorId,
  emailScriptSelectorId,
  emailRetrieverInstanceId = 999,
  emailAddress,
  pop3Server,
  pop3Port = 110,
  pop3Password,
  pop3LoginId,
  smtpPort = 25,
  smtpServer
}) {
  // provision chat queue
  try {
    console.log(`creating chat routing queue for department ${departmentName}...`)
    const results = await egain.queue.create({
      departmentName,
      callIdentifier: departmentName,
      queueName: 'chat',
      scriptSelectorId: chatScriptSelectorId,
      mrdId: chatMrdId,
      maxTaskLimit: 5000
    })
    // check results
    if (results.rowsAffected.length) {
      let bad = false
      for (const r of results.rowsAffected) {
        if (r === 0) {
          bad = true
        }
      }
      if (bad) {
        console.log(`failed to create chat routing queue for department ${departmentName}. rows affected:`, results.rowsAffected)
      } else {
        console.log(`successfully created chat routing queue for department ${departmentName}. results:`, JSON.stringify(results, null, 2))
      }
    } else {
      console.log(`failed to create chat routing queue for department ${departmentName}. No rows affected.`)
    }
  } catch (e) {
    console.log(`failed to create chat routing queue for department ${departmentName}:`, e.message)
    throw e
  }

  // provision chat entry point
  try {
    // create chat entry point associated with the chat queue
    console.log(`creating chat entry point for department ${departmentName}...`)
    const results = await egain.entryPoint.create({
      departmentName,
      entryPointName: 'chat',
      queueName: 'chat'
    })
    console.log(`successfully created chat entry point for department ${departmentName}. results:`, JSON.stringify(results, null, 2))
  } catch (e) {
    console.log(`failed to create chat entry point for department ${departmentName}:`, e.message)
    throw e
  }

  // provision email queue
  try {
    // create email queue
    console.log(`creating email routing queue for department ${departmentName}...`)
    const results = await egain.queue.create({
      departmentName,
      callIdentifier: departmentName,
      queueName: 'email',
      scriptSelectorId: emailScriptSelectorId,
      mrdId: emailMrdId,
      maxTaskLimit: 15000
    })
    console.log(`successfully created email routing queue for department ${departmentName}. results:`, JSON.stringify(results, null, 2))
  } catch (e) {
    console.log(`failed to create email routing queue for department ${departmentName}:`, e.message)
    // stop provision
    throw e
  }

  // provision email alias
  try {
    console.log(`creating email alias for department ${departmentName}...`)

    // create the email alias
    const results = await egain.alias.create({
      departmentName,
      pop3Password,
      emailAddress,
      aliasName: 'email',
      instanceId: emailRetrieverInstanceId, // rx-instance ID from eGMasterDB.dbo.EGPL_DSM_INSTANCE
      pop3Server,
      pop3Port,
      pop3LoginId,
      smtpPort,
      smtpServer
    })
    console.log(`successfully created email alias for department ${departmentName}. results:`, JSON.stringify(results, null, 2))
  } catch (e) {
    console.log(`failed to create email alias for department ${departmentName}:`, e.message)
    // stop provision
    throw e
  }

  // provision email workflow
  try {
    // create the workflow
    console.log(`creating email workflow for department ${departmentName}...`)
    const results = await egain.workflow.create({
      departmentName,
      workflowName: 'email',
      description: 'email workflow',
      queueName: 'email',
      aliasName: 'email'
    })
    console.log(`successfully created email workflow for department ${departmentName}. results:`, JSON.stringify(results, null, 2))
  } catch (e) {
    console.log(`failed to create email workflow for department ${departmentName}:`, e.message)
    // stop provision
    throw e
  }
}
