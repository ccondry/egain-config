const Egain = require('../../')
// create eGain interface object
const egain = new Egain({
  host: 'cceece.dcloud.cisco.com',
  username: 'sa',
  password: 'C1sco12345',
  db: 'eGActiveDB'
})

// user ID
const user = {
  id: '1000'
}
// constants that you should get from ICM, or maybe the EGICM_QUEUE table
const chatMrdId = 5004
const chatScriptSelectorId = 5065
const emailMrdId = 5005
const emailScriptSelectorId = 5066
const emailRetrieverInstanceId = 999
// the target department name
const departmentName = user.id

// run
main().catch(e => console.log(e.message))

async function main () {
  try {

    // generate ID for new chat queue
    const queueId = await egain.platform.getNextSequence({
      tableName: 'EGPL_ROUTING_QUEUE'
    })

    // create chat queue
    await egain.queue.create({
      queueId,
      queueName: 'chat',
      departmentId,
      callId: user.id
    })

    // generate ID for ACL
    const aclId = await egain.platform.getNextSequence({
      tableName: 'EGPL_USER_ACL'
    })

    // create ACL
    await egain.acl.create({
      aclId
      resourceId: queueId,
      resourceType: 1056, // 1056 = routing_queue type
      groupResource: 0, // this is a single resource, not a group resource
      departmentId: departmentId
      resourceId,
      resourceType,
      groupResource,
      departmentId
    })

    // create ACL permission
    await egain.aclPermission.create({
      aclId, // the ACL unique ID
      partyId: 1, // 1 = PA user
      permission: 7, // 7 = read/write/delete ?
      basePermission: 0 // 0 = none ?
    })

    // add routing queue watcher
    await egain.queue.addWatcher({
      queueId
    })

    // associate the chat queue with an ICM script selector
    await associateIcmScriptSelector({
      queueId,
      mrdId: chatMrdId,
      scriptSelectorId: chatScriptSelectorId,
      maxTaskLimit: 5000
    })

    // associate ECC Variable to chat queue
    await egain.icmQueue.associateEccVariable({
      queueId,
      eccVariableId: 1,
      callVariableId: 999
    })

    // try to fix the script selector
    // await fixIcmScriptSelector({
    //   queueId,
    //   mrdId: chatMrdId,
    //   scriptSelectorId: chatScriptSelectorId,
    //   maxTaskLimit: 5000
    // })
  } catch (e) {
    throw e
  }
}
