const Egain = require('../src')
// create eGain interface object
const egain = new Egain({
  host: 'cceece.dcloud.cisco.com',
  username: 'sa',
  password: 'C1sco12345',
  db: 'eGActiveDB'
})

describe('icmQueue.removeCtiVariables()', function () {
  it('should remove CTI var associations from a queue', function (done) {
    egain.icmQueue.removeCtiVariables({
      queueId: 1021
    })
    .then(() => {
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})

describe('icmQueue.associateCtiVariable()', function () {
  it('should add CTI var associations to a queue', function (done) {
    egain.icmQueue.associateCtiVariable({
      queueId: 1021,
      callVariableTag: 13,
      callVariableName: 'contact_point_data'
    })
    .then(() => {
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})
