// import the library. this library.
const Egain = require('../src')

// create eGain interface object by specifying the config
const egain = new Egain({
  host: 'cceece.dcloud.cisco.com',
  username: 'sa',
  password: 'C1sco12345',
  db: 'eGActiveDB'
})

// should work for all table names, unlike that OTHER function.
describe('platform.getNextSequence()', function () {
  it('should get the next sequence number for any specified table', function (done) {
    egain.platform.getNextSequence({
      tableName: 'EGPL_USER_ACL'
      // tableName: 'EGPL_ROUTING_QUEUE'
    })
    .then(seq => {
      console.log(`got next sequence: ${seq}.`)
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})

// does not work for all table names. I can't figure out why.
describe('platform.getNextSequence2()', function () {
  it('should get the next sequence number for specified table. does not work on some tables.', function (done) {
    egain.platform.getNextSequence2({
      tableName: 'EGPL_USER_ACL'
      // tableName: 'EGPL_ROUTING_QUEUE'
    })
    .then(seq => {
      console.log(`got next sequence: ${seq}.`)
      done()
    })
    .catch(e => {
      done(e)
    })
  })
})

// describe('platform.test()', function () {
//   it('should get the next sequence number for a table', function (done) {
//     egain.platform.test()
//     .then(seq => {
//       // console.log(`got next sequence: ${seq}.`)
//       done()
//     })
//     .catch(e => {
//       done(e)
//     })
//   })
// })
