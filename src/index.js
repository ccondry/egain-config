// dependencies
const sql = require('mssql')
// models
const Agent = require('./models/agent')
const Queue = require('./models/queue')
const Department = require('./models/department')
const IcmQueue = require('./models/icm-queue')
const ResourceType = require('./models/resource-type')
const ACL = require('./models/acl')
const ACLPermission = require('./models/acl-permission')
const Platform = require('./models/platform')
const Workflow = require('./models/workflow')
const Alias = require('./models/alias')
const SystemConfig = require('./models/system-config')
const EntryPoint = require('./models/entry-point')

class EgainConfig {
  constructor ({host, username, password, db = 'eGActiveDB'}) {
    // save input parameters to this object
    this.host = host
    this.db = db
    this.username = username
    this.password = password
    // create config object and save to this object
    this.config = {
      user: this.username,
      password: this.password,
      server: this.host,
      database: this.db
    }

    // these are the modules and we pass them the config object
    // these contain the usable functions that the user of this object would use
    this.agent = new Agent(this.config)
    this.queue = new Queue(this.config)
    this.department = new Department(this.config)
    this.icmQueue = new IcmQueue(this.config)
    this.resourceType = new ResourceType(this.config)
    this.acl = new ACL(this.config)
    this.aclPermission = new ACLPermission(this.config)
    this.platform = new Platform(this.config)
    this.workflow = new Workflow(this.config)
    this.alias = new Alias(this.config)
    this.systemConfig = new SystemConfig(this.config)
    this.entryPoint = new EntryPoint(this.config)

    // object type constants
    this.objectTypes = {
      emailAlias: 6,
      routingQueue: 1056,
      workflow: 1057,
      chatEntryPoint: 8078,
      dsmInstance: 8079
    }
  }
}

module.exports = EgainConfig
