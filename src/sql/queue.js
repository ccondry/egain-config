module.exports = {
  list () {
    return `SELECT [queue_id]
    ,[queue_name]
    ,[queue_state]
    ,[department_id]
    ,[queue_type]
    ,[queue_routing_type]
    ,[queue_chat_routing_type]
    ,[queue_skill_flag]
    ,[who_created]
    ,[when_created]
    ,[who_modified]
    ,[when_modified]
    ,[queue_description]
    ,[queue_link]
    ,[queue_roundrobin_index]
    ,[queue_level_1_age_time]
    ,[queue_level_2_age_time]
    ,[queue_level_3_age_time]
    ,[queue_priority]
    ,[queue_push_flag]
    ,[chat_default_transfer_queue]
    ,[default_kb_folder]
    ,[default_social_queue]
    ,[social_routing_type]
    ,[max_chat_queue_depth]
    ,[chat_queue_priority]
    ,[chat_alternate_engagement_time]
    ,[enable_video_chat]
    ,[delete_flag]
    ,[enable_chat_attachment]
    ,[chat_max_escalation]
    ,[enable_direct_audio_chat]
    ,[show_accept_audio_chat]
    ,[create_date]
    ,[update_version]
    ,[call_identifier]
    ,[chat_reconnect_timeout]
    ,[chat_msg_inactivity_timeout]
    FROM egpl_routing_queue`
  },
  find () {
    // SQL input parameters:
    // @department_name nvarchar - department name queue is in
    // @queue_name nvarchar(4000) - name of the queue
    //
    return `-- the ECE department ID
    DECLARE @department_id int

    -- get department ID by name
    SELECT @department_id = [department_id]
    FROM [egpl_department]
    WHERE [delete_flag] = 'n'
    AND [department_name] = @department_name

    SELECT [queue_id]
    ,[queue_name]
    ,[queue_state]
    ,[department_id]
    ,[queue_type]
    ,[queue_routing_type]
    ,[queue_chat_routing_type]
    ,[queue_skill_flag]
    ,[who_created]
    ,[when_created]
    ,[who_modified]
    ,[when_modified]
    ,[queue_description]
    ,[queue_link]
    ,[queue_roundrobin_index]
    ,[queue_level_1_age_time]
    ,[queue_level_2_age_time]
    ,[queue_level_3_age_time]
    ,[queue_priority]
    ,[queue_push_flag]
    ,[chat_default_transfer_queue]
    ,[default_kb_folder]
    ,[default_social_queue]
    ,[social_routing_type]
    ,[max_chat_queue_depth]
    ,[chat_queue_priority]
    ,[chat_alternate_engagement_time]
    ,[enable_video_chat]
    ,[delete_flag]
    ,[enable_chat_attachment]
    ,[chat_max_escalation]
    ,[enable_direct_audio_chat]
    ,[show_accept_audio_chat]
    ,[create_date]
    ,[update_version]
    ,[call_identifier]
    ,[chat_reconnect_timeout]
    ,[chat_msg_inactivity_timeout]
    FROM egpl_routing_queue
    WHERE QUEUE_NAME = @queue_name
    AND DEPARTMENT_ID = @department_id`
  },
  create () {
    // input
    // the ECE / ICM department name
    // @department_name nvarchar(32) = '1000'
    // the call identifier in ECE. doesn't get sent to ICM.
    // @call_identifier nvarchar(64) = '1006'
    // text name of the queue
    // @queue_name nvarchar(64) = 'chat'
    // ID of the ICM script selector to associate with queue
    // @script_selector_id int = 5065
    // ICM media routing domain ID
    // @mrd_id int = 5004
    // max task limit for the queue
    // @max_task_limit int = 15000
    return `BEGIN TRANSACTION [Tran1]

    BEGIN TRY

    -- variables used later
    -- the ECE department ID
    DECLARE @department_id int
    -- chat queue ID
    DECLARE @queue_id numeric(19, 0)
    -- ACL ID
    DECLARE @acl_id numeric(19, 0)

    -- variables used by stored proc
    DECLARE	@return_value int,
    @v_new_seq numeric(19, 0),
    @v_sql_code int,
    @v_sql_message nvarchar(1024)

    -- get department ID by name
    SELECT @department_id = [department_id]
    FROM [egpl_department]
    WHERE [delete_flag] = 'n'
    AND [department_name] = @department_name

    -- get a new sequence number for routing queue
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_ROUTING_QUEUE',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new queue ID
    SET @queue_id = @v_new_seq

    -- create new routing queue
    INSERT INTO egpl_routing_queue (
      queue_skill_flag, -- @P0
      chat_alternate_engagement_time,
      who_created,
      default_social_queue,
      enable_chat_attachment,
      queue_state,
      chat_queue_priority,
      queue_routing_type, -- @P7
      queue_level_2_age_time,
      chat_msg_inactivity_timeout,
      queue_chat_routing_type, -- @P10
      when_created, -- @P11
      queue_priority,
      enable_video_chat,
      delete_flag,
      social_routing_type,
      chat_default_transfer_queue,
      queue_id, -- @P17
      queue_link,
      who_modified,
      queue_type,
      department_id, -- @P21
      queue_description,
      call_identifier,
      queue_level_1_age_time,
      chat_reconnect_timeout,
      show_accept_audio_chat,
      queue_name,
      queue_roundrobin_index,
      default_kb_folder,
      chat_max_escalation, -- @P30
      queue_level_3_age_time,
      max_chat_queue_depth,
      enable_direct_audio_chat,
      queue_push_flag,
      when_modified -- @P35
    ) VALUES (
      0, -- queue_skill_flag
      -1, -- chat_alternate_engagement_time
      1, -- who_created
      0, -- default_social_queue
      0, -- enable_chat_attachment
      1, -- queue_state
      5, -- chat_queue_priority
      0, -- queue_routing_type
      0, -- queue_level_2_age_time
      0, -- chat_msg_inactivity_timeout
      0, -- queue_chat_routing_type
      GETUTCDATE(), -- when_created
      0, -- queue_priority
      0, -- enable_video_chat
      N'n', -- delete_flag
      0, -- social_routing_type
      0, -- chat_default_transfer_queue
      @queue_id, -- queue_id
      0, -- queue_link
      0, -- who_modified
      1, -- queue_type
      @department_id, -- department_id
      NULL, -- queue_description
      @call_identifier, -- call_identifier
      0, -- queue_level_1_age_time
      0, -- chat_reconnect_timeout
      1, -- show_accept_audio_chat
      @queue_name, -- queue_name
      0, -- queue_roundrobin_index
      0, -- default_kb_folder
      5, -- chat_max_escalation
      0, -- queue_level_3_age_time
      -1, -- max_chat_queue_depth
      1, -- enable_direct_audio_chat
      0, -- queue_push_flag
      NULL -- when_modified
    )

    -- get a new sequence number for ACL
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_USER_ACL',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new ACL ID
    SET @acl_id = @v_new_seq

    -- add ACL
    INSERT INTO egpl_user_acl (
      acl_id,
      resource_id,
      resource_type,
      group_resource,
      department_id,
      create_date
    ) VALUES (
      @acl_id,
      @queue_id,
      1056, -- 1056 = routing_queue type
      0, -- 0 = single resource, not group
      @department_id,
      GETUTCDATE()
    )

    -- add ACL permission
    INSERT INTO egpl_user_acl_permission (
      acl_id,
      party_id,
      permission,
      base_permission
    ) VALUES (
      @acl_id,
      1, -- 1 = PA user
      7, -- 7 = read/write/delete ?
      0 -- 0 = none ?
    )

    -- add routing queue watcher?
    INSERT INTO egpl_routing_queue_wat (
      instance_id,
      queue_id,
      when_modified
    ) VALUES (
      0,
      @queue_id,
      GETUTCDATE()
    )

    INSERT INTO EGICM_QUEUE (
      QUEUE_ID,
      MRD_ID,
      ROUTING_PRIORITY,
      QUEUE_PRIORITY,
      SCRIPT_SELECTOR_ID,
      MAX_WAIT_TIME,
      MAX_TASK_LIMIT
    ) VALUES (
      @queue_id,
      @mrd_id,
      0, -- routing priority
      0, -- queue priority
      @script_selector_id, -- ICM script selector ID
      NULL, -- max wait time
      @max_task_limit-- max task limit
    )

    INSERT INTO egicm_queue_ecc_variable (
      queue_id,
      ecc_variable_id,
      call_variable_id
    ) VALUES (
      @queue_id,
      1,
      999
    )


    COMMIT TRANSACTION [Tran1]

    END TRY

    BEGIN CATCH

    ROLLBACK TRANSACTION [Tran1]

    END CATCH`
  },
  setCallIdentifier () {
    // set the "call identifier" call variable for a queue
    // SQL input parameters:
    // @call_identifier nvarchar - string to use as call ID
    // @queue_id bigint - queue ID from egpl_routing_queue
    return `UPDATE egpl_routing_queue
    SET who_modified = 1, call_identifier = @call_identifier, when_modified = GETUTCDATE()
    WHERE queue_id = @queue_id`
  },
  getCallVariableMapping () {
    // get all call variable mapping info for a queue ID
    // inputs:
    // @queue_id bigint
    // @department_id bigint
    return `SELECT  egcti_call_variable_mapping.call_variable_id  ,
    egcti_call_variable_mapping.call_variable_object  ,
    egcti_call_variable_mapping.call_variable_name  ,
    egcti_call_variable_mapping.call_variable_custom_attribute  ,
    ISNULL (a.queue_id ,0) as ''QUEUE_ID''   ,
    ISNULL (a.call_variable_tag ,0) as ''CALL_VARIABLE_TAG'',
    CASE WHEN a.queue_id > 0 THEN 1 ELSE 0 END AS ''ISSELECTED''
    FROM   egcti_call_variable_mapping
    LEFT OUTER JOIN (
      SELECT  egcti_queue_call_variable.call_variable_id  ,
      egcti_queue_call_variable.call_variable_tag  ,
      egcti_queue_call_variable.queue_id  as ''QUEUE_ID''
      FROM  egcti_queue_call_variable
      WHERE  egcti_queue_call_variable.queue_id  =  @queue_id
      And  egcti_queue_call_variable.call_variable_tag  IN (
        13 , 14 , 15 , 16 , 17 , 18 , 19 , 20 , 21 , 22
      )
    ) a
    ON a.call_variable_id  =  egcti_call_variable_mapping.call_variable_id
    WHERE  egcti_call_variable_mapping.department_id  =  @department_id
    ORDER BY a.call_variable_tag`
  },
  deleteCallVariableMapping () {
    // delete all call variable mapping from a queue
    // inputs:
    // @queue_id bigint
    // call_variable_tags 13-22 are CallVariable1 - CallVariable10
    return `DELETE FROM egcti_queue_call_variable
    WHERE queue_id = @queue_id
    AND call_variable_tag IN (13, 14, 15, 16, 17, 18, 19, 20, 21, 22)`
  },
  addCallVariableMapping () {
    // add a single call variable mapping to a queue
    // 1024,983,13
    // inputs:
    // @queue_id bigint - the internal db ID of the chat/email queue
    // @call_variable_id bigint - the internal db ID of the call variable from EGCTI_CALL_VARIABLE table
    // @call_variable_tag bigint - the internal db ID of the call variable tag - value is 13 - 22 for CallVariable1 - CallVariable10
    return `INSERT INTO egcti_queue_call_variable (
      queue_id,
      call_variable_id,
      call_variable_tag
    ) VALUES (
      @queue_id,
      @call_variable_id,
      @call_variable_tag
    )`
  },
  listCallVariables () {
    // list all CTI call variables
    return `SELECT * FROM EGCTI_CALL_VARIABLE`
  },
  delete () {
    // puts a chat or email queue into deleted state
    // input:
    // @queue_id bigint - the internal db ID of the chat/email queue
    return `UPDATE egpl_routing_queue
    SET delete_flag = 'y',
    queue_name = (
      SELECT queue_name + '#deleted#' + CAST(queue_id as varchar(10))
      FROM egpl_routing_queue
      WHERE queue_id = @queue_id
    )
    WHERE queue_id = @queue_id`
  },
  addWatcher () {
    // adds a chat or email queue watcher
    // input:
    // @instance_id bigint - the instance ID
    // @queue_id bigint - the internal db ID of the chat/email queue
    return `INSERT INTO egpl_routing_queue_wat (
      instance_id,
      queue_id,
      when_modified
    ) VALUES (
      @instance_id,
      @queue_id,
      GETUTCDATE()
    )`
  }
}
