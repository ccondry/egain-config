module.exports = {
  list () {
    // list all chat entry points
    // inputs:
    // @department_id bigint - the department ID
    return `SELECT is_secure,
    entry_point_id,
    queue_id,
    wc_enable_collaboration,
    is_active,
    is_default,
    entry_point_description,
    entry_point_name
    FROM eglv_entry_point
    WHERE deptt_id = @department_id
    AND delete_flag = 'n'`
  },
  find () {
    // find chat entry point by queue ID
    // inputs:
    // @queue bigint - the chat queue ID
    return `SELECT is_secure,
    entry_point_id,
    queue_id,
    wc_enable_collaboration,
    is_active,
    is_default,
    entry_point_description,
    entry_point_name
    FROM eglv_entry_point
    WHERE delete_flag = 'n'
    AND queue_id = @queue_id`
  },
  create () {
    // create chat entry point
    // inputs:
    //
    // the ECE / ICM department name
    // @department_name nvarchar(32) = '1000'
    // the desired name for the new entry point
    // @entry_point_name nvarchar(32) = 'chat'
    // name of the existing chat queue in this department
    // @queue_name nvarchar(32) = 'chat'

    return `BEGIN TRANSACTION [Tran1]

    BEGIN TRY

    -- variables used later
    -- the ECE department ID
    DECLARE @department_id int
    -- chat entry point ID
    DECLARE @entry_point_id numeric(19, 0)
    -- chat queue ID
    DECLARE @queue_id numeric(19, 0)

    -- variables used by stored proc
    DECLARE	@return_value int,
    @v_new_seq numeric(19, 0),
    @v_sql_code int,
    @v_sql_message nvarchar(1024)

    -- get department ID by name
    SELECT @department_id = [department_id]
    FROM [egpl_department]
    WHERE [delete_flag] = 'n'
    AND [department_name] = @department_name

    -- get chat routing queue ID by name
    SELECT @queue_id = queue_id
    FROM egpl_routing_queue
    WHERE QUEUE_NAME = @queue_name
    AND DEPARTMENT_ID = @department_id

    -- get a new sequence number for routing queue
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGLV_ENTRY_POINT',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT

    -- new sequence is the new entry point ID
    SET @entry_point_id = @v_new_seq

    -- create chat entry point associated with the chat queue
    INSERT INTO eglv_entry_point (
      wc_page_push_customer,
      wc_follow_me_mode,
      wc_customer_exit,
      who_created,
      is_secure,
      deptt_id,
      wc_page_push_agent,
      wc_agent_popup,
      wc_browser_snap,
      entry_point_id,
      pass_cust_info_flag,
      when_created,
      append_transcript,
      delete_flag,
      queue_id,
      wc_enable_collaboration,
      authentication_mode,
      is_active,
      who_modified,
      wc_form_sync,
      send_transcript_flag_srv,
      message_id,
      is_default,
      entry_point_description,
      start_page_url,
      finish_page_url,
      wc_block_agent_actions,
      wc_cobrowse_only_interaction,
      template_set_id,
      send_transcript_flag_abn,
      agent_availability,
      console_mode,
      wc_limit_url,
      wc_block_customer_actions,
      wc_reconstruct_for_agent,
      entry_point_name,
      when_modified
    ) VALUES (
      0, -- wc_page_push_customer
      0, -- wc_follow_me_mode
      0, -- wc_customer_exit
      1, -- who_created
      0, -- is_secure
      @department_id, -- deptt_id
      1, -- wc_page_push_agent
      0, -- wc_agent_popup
      0, -- wc_browser_snap
      @entry_point_id, -- entry_point_id
      0, -- pass_cust_info_flag
      GETUTCDATE(), -- when_created
      0, -- append_transcript
      N'n', -- delete_flag
      @queue_id, -- queue_id
      0, -- wc_enable_collaboration
      1, -- authentication_mode
      1, -- is_active
      0, -- who_modified
      0, -- wc_form_sync
      0, -- send_transcript_flag_srv
      0, -- message_id
      0, -- is_default
      N'', -- entry_point_description
      NULL, -- start_page_url
      NULL, -- finish_page_url
      0, -- wc_block_agent_actions
      0, -- wc_cobrowse_only_interaction
      0, -- template_set_id
      0, -- send_transcript_flag_abn
      0, -- agent_availability
      0, -- console_mode
      0, -- wc_limit_url
      0, -- wc_block_customer_actions
      0, -- wc_reconstruct_for_agent
      @entry_point_name, -- entry_point_name
      NULL -- when_modified
    )

    --lock chat routing queue to the chat entry point
    INSERT INTO egpl_object_locking (
      locked_object_id,
      locked_object_type,
      locking_object_id,
      locking_object_type
    ) VALUES (
      @queue_id,
      1056, -- 1056 = routing queue object type
      @entry_point_id,
      8078 -- 8078 = entry point object type
    )

    -- lock DSM instance to chat entry point
    INSERT INTO egpl_object_locking (
      locked_object_id,
      locked_object_type,
      locking_object_id,
      locking_object_type
    ) VALUES (
      999, -- the DSM instance ID ?
      8079, -- 8079 = DSM instance object type
      @entry_point_id,
      8078 -- 8078 = entry point object type
    )

    -- create ICM entry point associated with chat entry point
    INSERT INTO egicm_entry_point (
      entry_point_id,
      subactivity,
      routing_type,
      cti_strategy
    ) VALUES (
      @entry_point_id,
      N'Chat',
      N'IPCC',
      N''
    )


    COMMIT TRANSACTION [Tran1]

    END TRY

    BEGIN CATCH

    ROLLBACK TRANSACTION [Tran1]

    END CATCH
    `
  },
  createIcmEntryPoint () {
    // this is used after creating the entry point
    // inputs:
    // @entry_point_id bigint - the ID of the entry point
    return `INSERT INTO egicm_entry_point (
      entry_point_id,
      subactivity,
      routing_type,
      cti_strategy
    ) VALUES (
      @entry_point_id,
      N'Chat',
      N'IPCC',
      N''
    )`
  }
}
