module.exports = {
  list () {
    // list ACLs
    return `SELECT acl_id,
    resource_id,
    resource_type,
    group_resource,
    department_id,
    update_version,
    create_date
    FROM egpl_user_acl`
  },
  find () {
    // find resources that a user has access to
    //
    // SQL input parameters:
    // @group_resource smallint - is group resource (vs. single resource). example 0 for single, 1 for group
    // @resource_type smallint - the resource type. example 1056
    // @resource_id bigint - the resource ID. example 1027
    // @department_id bigint - the department ID. example 1001
    //
    // example input 0,1056,1001
    return `SELECT acl_id,
    resource_id,
    resource_type,
    group_resource,
    department_id,
    update_version,
    create_date
    FROM egpl_user_acl
    WHERE group_resource = @group_resource
    AND resource_id = @resource_id
    AND resource_type = @resource_type
    AND department_id = @department_id`
  },
  create () {
    // create an access control list
    //
    // SQL input parameters:
    // @acl_id bigint - the new ACL ID. get from getNextSequence function.
    // @resource_id bigint - the resource ID. example 1027
    // @resource_type smallint - the resource type ID. example 1056
    // @group_resource smallint - is group resource (vs. single resource). example 0 for single, 1 for group
    // @department_id bigint - the department ID. example 1001
    //
    return `INSERT INTO egpl_user_acl (
      acl_id,
      resource_id,
      resource_type,
      group_resource,
      department_id,
      create_date
    ) VALUES (
      @acl_id,
      @resource_id,
      @resource_type,
      @group_resource,
      @department_id,
      GETUTCDATE()
    )`
  },
  delete () {
    // delete an access control list
    //
    // SQL input parameters:
    // @acl_id bigint - the ACL ID. example 222200000001429
    //
    return `DELETE FROM egpl_user_acl WHERE ACL_ID = @acl_id`
  }
}
