module.exports = {
  setProperty () {
    // set a config property
    return `INSERT INTO egpl_config_property (
      domain,
      name,
      value
    ) VALUES (
      @domain,
      @name,
      @value
    )`
  }
}
