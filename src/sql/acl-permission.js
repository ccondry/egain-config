module.exports = {
  list () {
    // list ACL permissions
    return `SELECT acl_id,
    party_id,
    permission,
    base_permission,
    update_version,
    create_date
    FROM egpl_user_acl_permission`
  },
  find () {
    // find resources that a user has access to
    //
    // SQL input parameters:
    // @acl_id bigint - acl_id from EGPL_USER_ACL table. example 222200000001394
    //
    return `SELECT acl_id,
    party_id,
    permission,
    base_permission,
    update_version,
    create_date
    FROM egpl_user_acl_permission
    WHERE acl_id = @acl_id`
  },
  // create () {
  //   // SQL input parameters:
  //   // @party_id int - example 1 (PA user)
  //   // @permission bigint - example 7
  //   // @base_permission int - example 0
  //   // @resource_id int - example 1027
  //   // @resource_type int - example 1056
  //   // @group_resource smallint - example 0
  //   //
  //   // example input 1,7,0,1027,1056,0
  //   return `INSERT INTO egpl_user_acl_permission (
  //     acl_id,
  //     party_id,
  //     permission,
  //     base_permission
  //   ) SELECT
  //     acl_id,
  //     @party_id,
  //     @permission,
  //     @base_permission,
  //     FROM egpl_user_acl
  //     WHERE resource_id = @resource_id
  //     AND resource_type = @resource_type
  //     AND group_resource = @group_resource
  //   )`
  // }
  create () {
    // SQL input parameters:
    // @acl_id bigint - example 222200000001429
    // @party_id bigint - example 1 (PA user)
    // @permission bigint - example 7
    // @base_permission bigint - example 0
    return `INSERT INTO egpl_user_acl_permission (
      acl_id,
      party_id,
      permission,
      base_permission
    ) VALUES (
      @acl_id,
      @party_id,
      @permission,
      @base_permission
    )`
  },
  update () {
    // SQL input parameters:
    // @acl_id bigint - example 222200000001429
    // @party_id bigint - example 1 (PA user)
    // @permission bigint - example 7
    // @base_permission bigint - example 0
    return `UPDATE egpl_user_acl_permission
    SET permission = @permission, base_permission = @base_permission
    WHERE acl_id = @acl_id
    AND party_id = @party_id`
  },
  delete () {
    // delete an access control list permission
    //
    // SQL input parameters:
    // @acl_id bigint - the ACL ID. example 222200000001429
    //
    return `DELETE FROM egpl_user_acl_permission WHERE ACL_ID = @acl_id`
  }
}
