module.exports = {
  getNextSequence () {
    // SQL input parameters:
    // @table_name
    //
    // SQL output parameters:
    // @v_new_seq bigint - the new sequence number
    // @v_sql_code int - SQL status code for query. 0 is success.
    // @v_sql_message nvarchar - SQL status text for errors. empty if success.
    //
    return `DECLARE	@return_value int,
    @v_new_seq numeric(19, 0),
    @v_sql_code int,
    @v_sql_message nvarchar(1024)

    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = @table_name,
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT

    SELECT @v_new_seq as v_new_seq,
    @v_sql_code as v_sql_code,
    @v_sql_message as v_sql_message`
  },
  lock () {
    // input
    // @locked_object_id bigint - the alias ID
    // @locked_object_type int - 6 for email alias type
    // @locking_object_id bigint - the workflow ID
    // @locking_object_type int - 1057 for workflow type
    // example input 1008,6,1039,1057
    // 1008,6,1042,1057
    // 
    // 1038,1056,1044,1057
    return `INSERT INTO egpl_object_locking (
      locked_object_id,
      locked_object_type,
      locking_object_id,
      locking_object_type
    ) VALUES (
      @locked_object_id,
      @locked_object_type,
      @locking_object_id,
      @locking_object_type
    )`
  }
}
