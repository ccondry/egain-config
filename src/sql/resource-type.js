module.exports = {
  list () {
    // list ACLs
    return `SELECT *
    FROM egpl_resource_type`
  },
  find () {
    // find resources that a user has access to
    //
    // SQL input parameters:
    // @resource_type_name nvarchar - name of the resource type to find
    //
    return `SELECT *
    FROM egpl_resource_type
    WHERE resource_type_name = @resource_type_name`
  }
}
