module.exports = {
  list () {
    return `SELECT department_id
    ,department_name
    ,department_desc
    ,delete_flag
    ,create_date
    FROM egpl_department
    WHERE delete_flag = 'n'`
  },
  find () {
    // SQL input parameters:
    // @department_name nvarchar(4000) - the exact department name to look for
    //
    return `SELECT [department_id]
    ,[department_name]
    ,[department_desc]
    ,[delete_flag]
    ,[create_date]
    FROM [egpl_department]
    WHERE [delete_flag] = 'n'
    AND [department_name] = @department_name`
  }
}
