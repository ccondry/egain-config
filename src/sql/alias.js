module.exports = {
  find () {
    return `SELECT pop3_password,
    pop3_server,
    mail_server_port,
    alias_id,
    smtp_login_id,
    recv_email_address,
    smtp_flag smtp_flag,
    mail_server_protocol,
    smtp_protocol,
    smtp_blackhole_email_address,
    smtp_password,
    pop3_blackhole_email_address,
    is_default_alias,
    delete_flag,
    last_modified,
    alias_description,
    who_modified,
    department_id,
    incoming_connection_type_flag,
    alias_name,
    pop3_login_id,
    instance_id,
    outgoing_connection_type_flag,
    smtp_port,
    auto_bcc,
    smtp_server,
    pop3_flag,
    alias_status
    FROM egml_mailhost
    WHERE alias_status IN (1, 2)
    AND delete_flag = 'n'
    AND recv_email_address = @recv_email_address`
  },
  list () {
    // list all email aliases
    // inputs:
    // @department_id bigint - the department ID
    return `SELECT pop3_password,
    pop3_server,
    mail_server_port,
    alias_id,
    smtp_login_id,
    recv_email_address,
    smtp_flag smtp_flag,
    mail_server_protocol,
    smtp_protocol,
    smtp_blackhole_email_address,
    smtp_password,
    pop3_blackhole_email_address,
    is_default_alias,
    delete_flag,
    last_modified,
    alias_description,
    who_modified,
    department_id,
    incoming_connection_type_flag,
    alias_name,
    pop3_login_id,
    instance_id,
    outgoing_connection_type_flag,
    smtp_port,
    auto_bcc,
    smtp_server,
    pop3_flag,
    alias_status
    FROM egml_mailhost
    WHERE alias_status IN (1, 2)
    AND delete_flag = 'n'
    AND department_id = @department_id
    ORDER BY recv_email_address ASC`
  },
  isInUse () {
    // validate that an email address does not already exist as an alias,
    // and also that the alias is not in use in that department
    //
    // inputs:
    // @email_address nvarchar(4000) - the name of the receiving email address to check
    // @alias_name nvarchar(4000) - the name of the email alias to check
    // @department_id bigint - the department ID
    //
    // outputs:
    // 0 if valid (address/alias are not in use yet)
    // 1 if invalid (address/alias are already in use)
    // N'support_0325@dcloud.cisco.com',N'0325',1000
    return `SELECT COUNT(*) AS count
    FROM egml_mailhost
    WHERE ( lower(recv_email_address) = lower(@email_address)
      OR ( lower(alias_name) = lower(@alias_name)
        AND department_id = @department_id
      )
    )
    AND delete_flag = 'n'`
  },
  create () {
    // -- the ECE / ICM department name
    // @department_name nvarchar(4000) = '1000'
    // -- encrypted email password - this one is C1sco12345
    // @pop3_password nvarchar(4000) = '3736363436353338363435353638343135343636363136363434373734333634333037413442353934443444343136363642373235303435353037373531353137353732364134333242363236383441373332463439334432333532343537363433333134353438333536353641354137343531353133443344'
    // -- the email address to use
    // @email_address nvarchar(4000) = 'support_' + @department_name + '@dcloud.cisco.com'
    // -- name of the alias
    // @alias_name nvarchar(4000) = 'email'
    // -- rx-instance ID from eGMasterDB.dbo.EGPL_DSM_INSTANCE
    // @instance_id int = 999
    // -- incoming mail server
    // @pop3_server nvarchar(4000) = 'branding.dcloud.cisco.com'
    // @pop3_port int = 110
    // @pop3_login_id nvarchar(4000) = 'support_' + @department_name
    // -- outgoing mail server
    // @smtp_port int = 25
    // @smtp_server nvarchar(4000) = 'branding.dcloud.cisco.com'

    return `BEGIN TRANSACTION [Tran1]

    BEGIN TRY

    -- variables used later
    -- the ECE department ID
    DECLARE @department_id int
    -- email alias ID
    DECLARE @alias_id numeric(19, 0)

    -- variables used by stored proc
    DECLARE	@return_value int,
    @v_new_seq numeric(19, 0),
    @v_sql_code int,
    @v_sql_message nvarchar(1024)

    -- get department ID by name
    SELECT @department_id = [department_id]
    FROM [egpl_department]
    WHERE [delete_flag] = 'n'
    AND [department_name] = @department_name

    -- get a new sequence number
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGML_MAILHOST',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new alias ID
    SET @alias_id = @v_new_seq

    -- create the email alias
    INSERT INTO egml_mailhost (
      pop3_password,
      pop3_server,
      mail_server_port,
      alias_id,
      smtp_login_id,
      recv_email_address,
      smtp_flag,
      mail_server_protocol,
      smtp_protocol,
      smtp_blackhole_email_address,
      smtp_password,
      pop3_blackhole_email_address,
      is_default_alias,
      delete_flag,
      last_modified,
      alias_description,
      who_modified,
      department_id,
      incoming_connection_type_flag,
      alias_name,
      pop3_login_id,
      instance_id,
      outgoing_connection_type_flag,
      smtp_port,
      auto_bcc,
      smtp_server,
      pop3_flag,
      alias_status
    ) VALUES (
      @pop3_password, -- pop3_password
      @pop3_server, -- pop3_server
      @pop3_port, -- mail_server_port
      @alias_id, -- alias_id
      NULL, -- smtp_login_id
      @email_address, -- recv_email_address
      1, -- smtp_flag
      2, -- mail_server_protocol
      2, -- smtp_protocol
      NULL, -- smtp_blackhole_email_address
      NULL, -- smtp_password
      NULL, -- pop3_blackhole_email_address
      0, -- is_default_alias
      N'n', -- delete_flag
      GETUTCDATE(), -- last_modified
      NULL, -- alias_description
      1, -- who_modified
      @department_id, -- department_id
      0, -- incoming_connection_type_flag
      @alias_name, -- alias_name
      @pop3_login_id, -- pop3_login_id
      @instance_id, -- instance_id
      0, -- outgoing_connection_type_flag
      @smtp_port, -- smtp_port
      NULL, -- auto_bcc
      @smtp_server, -- smtp_server
      -1, -- pop3_flag
      1 -- alias_status
    )

    -- set system config properties for email alias
    INSERT INTO egpl_config_property (
      domain,
      name,
      value
    ) VALUES (
      'rx.alias.' + @email_address,
      'folder',
      'inbox'
    )
    INSERT INTO egpl_config_property (
      domain,
      name,
      value
    ) VALUES (
      'rx.alias.' + @email_address,
      'incoming_debug_flag',
      '0'
    )
    INSERT INTO egpl_config_property (
      domain,
      name,
      value
    ) VALUES (
      'dx.alias.' + @email_address,
      'outgoing_debug_flag',
      '0'
    )

    COMMIT TRANSACTION [Tran1]

    END TRY

    BEGIN CATCH

    ROLLBACK TRANSACTION [Tran1]

    END CATCH`
  }
}
