module.exports = {
  list () {
    // list ICM queue associations
    return `SELECT [QUEUE_ID]
    ,[MRD_ID]
    ,[ROUTING_PRIORITY]
    ,[QUEUE_PRIORITY]
    ,[SCRIPT_SELECTOR_ID]
    ,[MAX_WAIT_TIME]
    ,[MAX_TASK_LIMIT]
    FROM EGICM_QUEUE`
  },
  find () {
    // find ICM queue association for given queue ID
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID
    return `SELECT [QUEUE_ID]
    ,[MRD_ID]
    ,[ROUTING_PRIORITY]
    ,[QUEUE_PRIORITY]
    ,[SCRIPT_SELECTOR_ID]
    ,[MAX_WAIT_TIME]
    ,[MAX_TASK_LIMIT]
    FROM EGICM_QUEUE
    WHERE [QUEUE_ID] = @queue_id`
  },
  create () {
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID. example 1027
    // @mrd_id bigint - the ICM Media Routing Domain (MRD) to route in. example 5004
    // @script_selector_id bigint - ICM script selector ID to route with. example 5065
    // @max_task_limit - max tasks for this queue. example 5000 for chat, 15000 for email
    //
    // create a chat queue
    return `INSERT INTO EGICM_QUEUE (
      QUEUE_ID,
      MRD_ID,
      ROUTING_PRIORITY,
      QUEUE_PRIORITY,
      SCRIPT_SELECTOR_ID,
      MAX_WAIT_TIME,
      MAX_TASK_LIMIT
    ) VALUES (
      @queue_id,
      @mrd_id,
      0, -- routing priority
      0, -- queue priority
      @script_selector_id, -- ICM script selector ID
      NULL, -- max wait time
      @max_task_limit -- max task limit
    )`
  },
  update () {
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID. example 1027
    // @mrd_id bigint - the ICM Media Routing Domain (MRD) to route in. example 5004
    // @script_selector_id bigint - ICM script selector ID to route with. example 5065
    // @max_task_limit - max tasks for this queue. example 5000 for chat, 15000 for email
    //
    // create a chat queue
    return `UPDATE egicm_queue
    SET mrd_id = @mrd_id,
      routing_priority = 0,
      queue_priority = 0,
      script_selector_id = @script_selector_id,
      max_wait_time = NULL,
      max_task_limit = @max_task_limit
    WHERE queue_id = @queue_id`
  },
  delete () {
    // remove an ICM queue association
    //
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID. example 1027
    return `DELETE FROM EGICM_QUEUE WHERE QUEUE_ID = @queue_id`
  },
  removeCtiVariables () {
    // deletes association of CCE CTI call variables 1 - 10 from an egain queue.
    // use before replacing call variable associations.
    // @queue_id bigint - the egain queue ID. example 1027
    return `DELETE FROM egcti_queue_call_variable
    WHERE queue_id = @queue_id
    AND call_variable_tag IN (13, 14, 15, 16, 17, 18, 19, 20, 21, 22)`
  },
  associateCtiVariable () {
    // associate an ECC variable to a call variable
    //
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID. example 1027
    // @call_variable_name nvarchar - the name of the call variable. example contact_point_data
    // @call_variable_tag int - the ID of the ECC variable. 13 = callvar1, 22 = callvar10
    return `DECLARE @call_variable_id int
    SELECT @call_variable_id = [call_variable_id]
    FROM [egcti_call_variable]
    WHERE [call_variable_name] = @call_variable_name

    INSERT INTO egcti_queue_call_variable (
      queue_id,
      call_variable_id,
      call_variable_tag
    ) VALUES (
      @queue_id,
      @call_variable_id,
      @call_variable_tag
    )`

  },
  mapPq () {
    // map a CCE Precision Queue to an egain routing queue
    //
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID. example 1031
    // @pq_id int - the ID of the CCE precision queue. example 5045
    return `INSERT INTO egicm_queue_pq_mapped (
      queue_id,
      pq_id
    ) VALUES (
      @queue_id,
      @pq_id
    )`
  },
  unmapPq () {
    // remove a map of CCE Precision Queue to an egain routing queue
    //
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID. example 1031
    return `DELETE FROM egicm_queue_pq_mapped
    WHERE queue_id = @queue_id`
  },
  mapSg () {
    // map a CCE Skill Group to an egain routing queue
    //
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID. example 1031
    // @skill_target_id int - the ID of the CCE skill group (skill target). example 5045
    return `INSERT INTO egicm_queue_sg_mapped (
      queue_id,
      skill_target_id
    ) VALUES (
      @queue_id,
      @skill_target_id
    )`
  },
  unmapSg () {
    // remove a map of CCE Precision Queue to an egain routing queue
    //
    // SQL input parameters:
    // @queue_id bigint - the eGain queue ID. example 1031
    return `DELETE FROM egicm_queue_sg_mapped
    WHERE queue_id = @queue_id`
  },
}
