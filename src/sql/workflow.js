module.exports = {
  find () {
    // find a workflow
    // inputs:
    // @workflow_type = 1
    // @department_id bigint - the department ID
    return `-- the ECE department ID
    DECLARE @department_id int

    -- get department ID by name
    SELECT @department_id = [department_id]
    FROM [egpl_department]
    WHERE [delete_flag] = 'n'
    AND [department_name] = @department_name

    SELECT workflow_id,
    who_modified,
    workflow_name,
    department_id,
    who_created,
    workflow_description,
    workflow_type,
    ruleset_xml,
    workflow_active,
    rule_xml,
    when_created,
    workflow_sub_type,
    delete_flag,
    when_modified,
    workflow_xml
    FROM egpl_workflow_rule_xml
    WHERE department_id = @department_id
    AND workflow_name = @workflow_name
    AND delete_flag = 'N'`
  },
  list () {
    // list all workflows
    // inputs:
    // @workflow_type = 1
    // @department_id bigint - the department ID
    return `SELECT workflow_id,
    who_modified,
    workflow_name,
    department_id,
    who_created,
    workflow_description,
    workflow_type,
    ruleset_xml,
    workflow_active,
    rule_xml,
    when_created,
    workflow_sub_type,
    delete_flag,
    when_modified,
    workflow_xml
    FROM egpl_workflow_rule_xml
    WHERE workflow_type = @workflow_type
    AND department_id = @department_id
    AND delete_flag = 'N'
    ORDER BY workflow_name`
  },
  create () {
    // creates an email queue that just receives email from an alias and sends
    // it to an email queue
    //
    // input
    // the ECE / ICM department name
    // @department_name nvarchar(32) = '1000'
    // desired workflow name
    // @workflow_name nvarchar(64) = 'email'
    // email routing queue name
    // @queue_name nvarchar(64) = 'email'
    // email alias name
    // @alias_name nvarchar(64) = 'email'
    // workflow description
    // @description nvarchar(1024) = 'email workflow'

    return `BEGIN TRANSACTION [Tran1]

    BEGIN TRY

    -- variables used later
    -- the ECE department ID
    DECLARE @department_id int
    -- email alias ID
    DECLARE @alias_id int
    -- email queue ID
    DECLARE @queue_id int

    -- get department ID by name
    SELECT @department_id = [department_id]
    FROM [egpl_department]
    WHERE [delete_flag] = 'n'
    AND [department_name] = @department_name

    -- get routing queue ID by name
    SELECT @queue_id = queue_id
    FROM egpl_routing_queue
    WHERE QUEUE_NAME = @queue_name
    AND DEPARTMENT_ID = @department_id

    -- get alias ID by email_address
    SELECT @alias_id = alias_id
    FROM egml_mailhost
    WHERE alias_name = @alias_name
    AND department_id = @department_id


    -- generated values from get_next_seq
    -- email workflow ID
    DECLARE @workflow_id int
    -- email workflow_item 1 ID
    DECLARE @workflow_item_id_1 int
    -- email workflow_item 2 ID
    DECLARE @workflow_item_id_2 int
    -- rule 1 ID
    DECLARE @rule_id_1 int
    -- rule 2 ID
    DECLARE @rule_id_2 int
    -- ruleset 1 ID
    DECLARE @rule_set_id_1 int
    -- ruleset 2 ID
    DECLARE @rule_set_id_2 int

    -- variables used by get_next_seq stored function
    DECLARE	@return_value int,
    @v_new_seq numeric(19, 0),
    @v_sql_code int,
    @v_sql_message nvarchar(1024)

    -- get a new sequence number for workflow ID
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_WORKFLOW',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new workflow ID
    SET @workflow_id = @v_new_seq

    -- get a new sequence number for workflow element in XML
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_WORKFLOW_ITEM',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new rule ID
    SET @workflow_item_id_1 = @v_new_seq

    -- get a new sequence number for workflow element in XML
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_WORKFLOW_ITEM',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new rule ID
    SET @workflow_item_id_2 = @v_new_seq

    -- get a new sequence number for first rule
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_RULE',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new rule ID
    SET @rule_id_1 = @v_new_seq

    -- get a new sequence number for second rule
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_RULE',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new rule ID
    SET @rule_id_2 = @v_new_seq

    -- get a new sequence number for first ruleset
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_RULE_SET',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new ruleset ID
    SET @rule_set_id_1 = @v_new_seq

    -- get a new sequence number for second ruleset
    EXEC	@return_value = [dbo].[egpl_f_get_next_seq]
    @v_table_name = 'EGPL_RULE_SET',
    @v_new_seq = @v_new_seq OUTPUT,
    @v_sql_code = @v_sql_code OUTPUT,
    @v_sql_message = @v_sql_message OUTPUT
    -- new sequence is the new ruleset ID
    SET @rule_set_id_2 = @v_new_seq

    -- create workflow xml
    INSERT INTO egpl_workflow_rule_xml (
      workflow_id,
      who_modified,
      workflow_name,
      department_id,
      who_created,
      workflow_description,
      workflow_type,
      ruleset_xml,
      workflow_active,
      rule_xml,
      when_created,
      workflow_sub_type,
      delete_flag,
      when_modified,
      workflow_xml
    ) VALUES (
      @workflow_id, -- workflow_id
      1, -- who_modified
      @workflow_name, -- workflow_name
      @department_id, -- department_id
      1, -- who_created
      @description, -- workflow_description
      1, -- workflow_type
      N'<?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE RuleSet>
      <RuleSet Id="' + CAST(@rule_set_id_1 as varchar(10)) + N'" Type="0" Active="FALSE">
        <RuleSetRule Id="' + CAST(@rule_id_1 as varchar(10)) + N'"/>
      </RuleSet><?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE RuleSet>
      <RuleSet Id="' + CAST(@rule_set_id_2 as varchar(10)) + N'" Type="0" Active="FALSE">
        <RuleSetRule Id="' + CAST(@rule_id_2 as varchar(10)) + N'"/>
      </RuleSet>', -- ruleset_xml
      1, -- workflow_active
      N'<?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE Rule>
      <Rule Id="' + CAST(@rule_id_1 as varchar(10)) + N'" Name="" Type="CONTINUE" Active="FALSE">
        <Action Id="-1" Type="ROUTE" FieldId="1" FieldType="LONG" RValue="' + CAST(@workflow_item_id_2 as varchar(10)) + N'" RValueType="LITERAL" R1Value="" ActionOPType="-1"/>
      </Rule><?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE Rule>
      <Rule Id="' + CAST(@rule_id_2 as varchar(10)) + N'" Name="" Type="CONTINUE" Active="FALSE">
        <Action Id="-1" Type="ROUTE" FieldId="2" FieldType="LONG" RValue="' + CAST(@queue_id as varchar(10)) + N'" RValueType="LITERAL" R1Value="" ActionOPType="-1"/>
      </Rule>	', -- rule_xml
      GETUTCDATE(), -- when_created
      1, -- workflow_sub_type
      N'N', -- delete_flag
      GETUTCDATE(), -- when_modified
      N'<?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE Workflow>
      <Workflow Id="' + CAST(@workflow_id as varchar(10)) + N'" Name="email" Description="' + @description + N'" Type="INBOUND" SubType="EMAIL" Active="TRUE" StartingItemId="' + CAST(@workflow_item_id_1 as varchar(10)) + N'">
        <WorkflowAssociation WorkflowAssocationId="-1" AssociationId="' + CAST(@alias_id as varchar(10)) + N'" AssociationType="1"/>
        <Node XPos="50" YPos="50">
          <Item Id="' + CAST(@workflow_item_id_1 as varchar(10)) + N'" Name="Start" Type="START" RuleSetId="' + CAST(@rule_set_id_1 as varchar(10)) + N'"/>
        </Node>
        <Node XPos="150" YPos="50">
          <Item Id="' + CAST(@workflow_item_id_2 as varchar(10)) + N'" Name="email" Type="QUEUE" RuleSetId="' + CAST(@rule_set_id_2 as varchar(10)) + N'"/>
        </Node>
        <Edge Type="CONTINUE" SrcItemId="' + CAST(@workflow_item_id_1 as varchar(10)) + N'" SrcRegionId="0" DestItemId="' + CAST(@workflow_item_id_2 as varchar(10)) + N'" DestRegionId="0" RuleId="' + CAST(@rule_id_1 as varchar(10)) + N'"/>
      </Workflow>' -- workflow_xml
    )

    --  lock alias to this workflow
    INSERT INTO egpl_object_locking (
      locked_object_id,
      locked_object_type,
      locking_object_id,
      locking_object_type
    ) VALUES (
      @alias_id,
      6, -- 6 = emailAlias object type
      @workflow_id,
      1057 -- 1057 = workflow object type
    )

    --  lock ICM queue to this workflow
    INSERT INTO egpl_object_locking (
      locked_object_id,
      locked_object_type,
      locking_object_id,
      locking_object_type
    ) VALUES (
      @queue_id,
      1056, -- 1056 = routingQueue object type
      @workflow_id,
      1057 -- 1057 = workflow object type
    )

    COMMIT TRANSACTION [Tran1]

    END TRY

    BEGIN CATCH

    ROLLBACK TRANSACTION [Tran1]

    END CATCH
`
  }
}
