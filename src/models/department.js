const mssql = require('mssql')
const queries = require('../sql/department')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list all departments
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async find ({name}) {
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('department_name', mssql.VarChar, name)
      .query(query)
      try {
        // has results
        return results.recordsets[0][0]
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }
}
