const mssql = require('mssql')
const queries = require('../sql/alias')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list email aliases
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // returns whether an email alias is associated with a workflow or not
  async isInUse ({
    emailAddress,
    aliasName,
    departmentId
  }) {
    const query = queries.isInUse()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('email_address', mssql.VarChar, emailAddress)
      .input('alias_name', mssql.VarChar, aliasName)
      .input('department_id', mssql.BigInt, departmentId)
      .query(query)
      try {
        // has results
        return results.recordset.count !== 0
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // find an email alias
  async find ({
    emailAddress
  }) {
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('recv_email_address', mssql.VarChar, emailAddress)
      .query(query)
      try {
        // has results
        return results.recordset[0]
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // create an email alias
  async create ({
    departmentName,
    pop3Password,
    emailAddress,
    aliasName,
    instanceId,
    pop3Server,
    pop3Port,
    pop3LoginId,
    smtpPort,
    smtpServer
  }) {
    // input
    // -- the ECE / ICM department name
    // @department_name nvarchar(4000) = '1000'
    // -- encrypted email password - this one is C1sco12345
    // @pop3_password nvarchar(4000) = '3736363436353338363435353638343135343636363136363434373734333634333037413442353934443444343136363642373235303435353037373531353137353732364134333242363236383441373332463439334432333532343537363433333134353438333536353641354137343531353133443344'
    // -- the email address to use
    // @email_address nvarchar(4000) = 'support_' + @department_name + '@dcloud.cisco.com'
    // -- name of the alias
    // @alias_name nvarchar(4000) = 'email'
    // -- rx-instance ID from eGMasterDB.dbo.EGPL_DSM_INSTANCE
    // @instance_id int = 999
    // -- incoming mail server
    // @pop3_server nvarchar(4000) = 'branding.dcloud.cisco.com'
    // @pop3_port int = 110
    // @pop3_login_id nvarchar(4000) = 'support_' + @department_name
    // -- outgoing mail server
    // @smtp_port int = 25
    // @smtp_server nvarchar(4000) = 'branding.dcloud.cisco.com'

    // validate input
    // if (!resourceId || !resourceType || !departmentId) {
    //   throw Error('resourceId, resourceType, and departmentId are required parameters for acl.create. They should be provided as properties of a single input object.')
    // }
    const query = queries.create()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('department_name', mssql.NVarChar(4000), departmentName)
      .input('pop3_password', mssql.NVarChar(4000), pop3Password)
      .input('email_address', mssql.NVarChar(4000), emailAddress)
      .input('alias_name', mssql.NVarChar(4000), aliasName)
      .input('instance_id', mssql.Int, instanceId)
      .input('pop3_server', mssql.NVarChar(4000), pop3Server)
      .input('pop3_port', mssql.Int, pop3Port)
      .input('pop3_login_id', mssql.NVarChar(4000), pop3LoginId)
      .input('smtp_port', mssql.Int, smtpPort)
      .input('smtp_server', mssql.NVarChar(4000), smtpServer)
      .query(query)
      // done
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

}
