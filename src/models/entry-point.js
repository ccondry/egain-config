const mssql = require('mssql')
const queries = require('../sql/entry-point')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async find ({
    queueId
  }) {
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .query(query)
      try {
        // has results
        return results.recordset[0]
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async create ({
    departmentName,
    entryPointName,
    queueName
  }) {
    // the ECE / ICM department name
    // @department_name nvarchar(32) = '1000'
    // the desired name for the new entry point
    // @entry_point_name nvarchar(32) = 'chat'
    // name of the existing chat queue in this department
    // @queue_name nvarchar(32) = 'chat'

    // validate input
    if (!departmentName || !entryPointName || !queueName) {
      throw Error('departmentName, entryPointName, and queueName are required parameters for entryPoint.create.')
    }
    const query = queries.create()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('department_name', mssql.NVarChar(4000), departmentName)
      .input('entry_point_name', mssql.NVarChar(4000), entryPointName)
      .input('queue_name', mssql.NVarChar(4000), queueName)
      .query(query)
      // done
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async createIcmEntryPoint ({
    entryPointId
  }) {
    // @entry_point_id bigint - the entry point ID
    // validate input
    if (!entryPointId) {
      throw Error('entryPointId is a required parameter for entryPoint.createIcmEntryPoint.')
    }
    const query = queries.createIcmEntryPoint()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('entry_point_id', mssql.BigInt, entryPointId)
      .query(query)
      // done
      return
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // async delete ({
  //   aclId
  // }) {
  //   // validate input
  //   if (!aclId) {
  //     throw Error('aclId is a required parameter for acl.delete.')
  //   }
  //   const query = queries.delete()
  //   try {
  //     const pool = await new mssql.ConnectionPool(this.config).connect()
  //     const results = await pool.request()
  //     .input('acl_id', mssql.BigInt, aclId)
  //     .query(query)
  //     // done
  //     return
  //   } catch (e) {
  //     throw e
  //   } finally {
  //     mssql.close()
  //   }
  // }
}
