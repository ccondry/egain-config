const mssql = require('mssql')
const queries = require('../sql/system-config')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async setProperty ({
    domain,
    name,
    value
  }) {
    const query = queries.setProperty()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('domain', mssql.NVarChar, domain)
      .input('name', mssql.NVarChar, name)
      .input('value', mssql.NVarChar, value)
      .query(query)
      try {
        // has results
        return results.recordset.count !== 0
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

}
