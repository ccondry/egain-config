const mssql = require('mssql')
const queries = require('../sql/acl-permission')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list ACLs
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async find ({
    aclId
  }) {
    // find ACLs
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('acl_id', mssql.BigInt, aclId)
      .query(query)
      try {
        // has results
        return results.recordset[0]
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async create ({
    aclId,
    partyId,
    permission,
    basePermission
  }) {
    const query = queries.create()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('acl_id', mssql.BigInt, aclId)
      .input('party_id', mssql.Int, partyId)
      .input('permission', mssql.Int, permission)
      .input('base_permission', mssql.Int, basePermission)
      .query(query)
      // done
      return
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async update ({
    aclId,
    partyId,
    permission,
    basePermission
  }) {
    const query = queries.update()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('acl_id', mssql.BigInt, aclId)
      .input('party_id', mssql.Int, partyId)
      .input('permission', mssql.Int, permission)
      .input('base_permission', mssql.Int, basePermission)
      .query(query)
      // done
      return
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // delete an ACL permission
  async delete ({
    aclId
  }) {
    // validate input
    if (!aclId) {
      throw Error('aclId is a required parameter for aclPermission.delete.')
    }
    const query = queries.delete()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('acl_id', mssql.BigInt, aclId)
      .query(query)
      // done
      return
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }
}
