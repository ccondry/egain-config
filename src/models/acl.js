const mssql = require('mssql')
const queries = require('../sql/acl')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list ACLs
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async find ({
    resourceId,
    resourceType,
    groupResource,
    departmentId
  }) {
    // find ACLs
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('resource_type', mssql.Int, resourceType)
      .input('group_resource', mssql.Int, groupResource)
      .input('department_id', mssql.Int, departmentId)
      .input('resource_id', mssql.Int, resourceId)
      .query(query)
      try {
        // has results
        return results.recordsets[0][0]
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async create ({
    aclId,
    resourceId,
    resourceType,
    groupResource = 0,
    departmentId
  }) {
    // validate input
    if (!resourceId || !resourceType || !departmentId) {
      throw Error('resourceId, resourceType, and departmentId are required parameters for acl.create. They should be provided as properties of a single input object.')
    }
    // list all queues
    const query = queries.create()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('acl_id', mssql.BigInt, aclId)
      .input('resource_id', mssql.Int, resourceId)
      .input('resource_type', mssql.Int, resourceType)
      .input('group_resource', mssql.Int, groupResource)
      .input('department_id', mssql.Int, departmentId)
      .query(query)
      // done
      return
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // delete an ACL
  async delete ({
    aclId
  }) {
    // validate input
    if (!aclId) {
      throw Error('aclId is a required parameter for acl.delete.')
    }
    const query = queries.delete()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('acl_id', mssql.BigInt, aclId)
      .query(query)
      // done
      return
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }
}
