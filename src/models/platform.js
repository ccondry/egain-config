const mssql = require('mssql')
const queries = require('../sql/platform')

module.exports = class {
  // copy the config to this object
  constructor (config) {
    this.config = config
  }

  // set locking/ownership of an object
  // object types:
  // 6 = email alias
  // 1009 = chat entry point
  // 1056 = routing queue
  // 1057 = workflow

  async lock ({
    lockedObjectId,
    lockedObjectType,
    lockingObjectId,
    lockingObjectType
  }) {
    try {
      const query = queries.lock()
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('locked_object_id', lockedObjectId)
      .input('locked_object_type', lockedObjectType)
      .input('locking_object_id', lockingObjectId)
      .input('locking_object_type', lockingObjectType)
      .query(query)
      return
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // getNextSequence gets the next valid sequential row ID for a table, like a
  // manual version of an auto-increment ID column.
  // you would use this function before inserting a row into a table.
  async getNextSequence ({tableName}) {
    // get the next valid database ID for a given table name
    try {
      const query = queries.getNextSequence()
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('table_name', tableName)
      .query(query)
      // console.log(results.recordset)
      return results.recordset[0].v_new_seq
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // getNextSequence2 gets the next valid sequential row ID for a table, like a
  // manual version of an auto-increment ID column.
  // you would use this function before inserting a row into a table.
  // this method does not work for some tableName inputs, like EGPL_USER_ACL
  // use the getNextSequence function instead
  async getNextSequence2 ({tableName}) {
    // get the next valid database ID for a given table name
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const req = pool.request()
      .input('v_table_name', mssql.VarChar, tableName)
      .output('v_new_seq', mssql.Int)
      .output('v_sql_code', mssql.Int)
      .output('v_sql_message', mssql.VarChar)

      const results1 = await req.execute('egpl_f_get_next_seq')
      // await sleep(2000)
      // console.log('req', req)
      // console.log('asdf', req.parameters.v_new_seq)
      console.log('results1', results1)
      const newSequence = results1.output.v_new_seq
      console.log('newSequence', newSequence)
      return newSequence
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }
}
