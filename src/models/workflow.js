const mssql = require('mssql')
const queries = require('../sql/workflow')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list all workflow
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async find ({
    workflowName,
    departmentName
  }) {
    // find a workflow
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('workflow_name', mssql.NVarChar, workflowName)
      .input('department_name', mssql.NVarChar, departmentName)
      .query(query)
      try {
        // has results
        return results.recordset[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async create ({
    departmentName,
    workflowName,
    queueName,
    aliasName,
    description = ''
  }) {
    // create an email workflow
    const query = queries.create()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      //
      // input
      // the ECE / ICM department name
      // @department_name nvarchar(32) = '1000'
      // desired workflow name
      // @workflow_name nvarchar(64) = 'email'
      // email routing queue name
      // @queue_name nvarchar(64) = 'email'
      // email alias name
      // @alias_name nvarchar(64) = 'email'
      // workflow description
      // @description nvarchar(1024) = 'email workflow'
      const results = await pool.request()
      .input('department_name', mssql.NVarChar(64), departmentName)
      .input('workflow_name', mssql.NVarChar(4000), workflowName)
      .input('queue_name', mssql.NVarChar(4000), queueName)
      .input('alias_name', mssql.NVarChar(4000), aliasName)
      .input('description', mssql.NVarChar(4000), description)
      .query(query)
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }
}
