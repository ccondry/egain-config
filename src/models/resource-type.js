const mssql = require('mssql')
const queries = require('../sql/resource-type')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list all resource types
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async find ({resourceTypeName}) {
    // find resource type
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('resource_type_name', mssql.Int, resourceTypeName)
      .query(query)
      try {
        // has results
        return results.recordsets[0][0]
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }
}
