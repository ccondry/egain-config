const mssql = require('mssql')
const queries = require('../sql/queue')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list all queues
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      // .input('queue_id', mssql.Int, queueId)
      // .input('call_identifier', mssql.VarChar, callId)
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async setCallIdentifier ({queueId, callId}) {
    // validate input
    if (!queueId || !callId || !callId.trim().length) {
      throw Error('queueId and callId are required parameters for queue.setCallIdentifier. They should be provided as properties of a single input object.')
    }
    // set the call identifier call variable for a queue
    const query = queries.setCallIdentifier()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .input('call_identifier', mssql.VarChar, callId)
      .query(query)
      if (results.rowsAffected[0] === 1) {
        // successfully modified queue
        return
      } else if (results.rowsAffected[0] === 0) {
        // no queue found by that ID
        throw Error('No queue was modified. queueId used was ' + queueId)
      } else {
        // more than one queue was modified? that should not happen...
        throw Error(`More than one queue with ID ${queueId} was modified! This should not happen! Check ${this.config.db} on ${this.config.host} for duplicate queues with ID ${queueId}.`)
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async create ({
    departmentName,
    callIdentifier,
    queueName,
    scriptSelectorId,
    mrdId,
    maxTaskLimit
  }) {
    // input
    // the ECE / ICM department name
    // @department_name nvarchar(32) = '1000'
    // the call identifier in ECE. doesn't get sent to ICM.
    // @call_identifier nvarchar(64) = '1006'
    // text name of the queue
    // @queue_name nvarchar(64) = 'chat'
    // ID of the ICM script selector to associate with queue
    // @script_selector_id int = 5065
    // ICM media routing domain ID
    // @mrd_id int = 5004
    // max task limit for the queue
    // @max_task_limit int = 15000
    // validate input
    // if (!queueId || !departmentId || !queueName.trim().length) {
    //   throw Error('queueId, departmentId, and queueName are required parameters for queue.create. They should be provided as properties of a single input object.')
    // }
    // list all queues
    const query = queries.create()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('department_name', mssql.NVarChar(64), departmentName)
      .input('call_identifier', mssql.NVarChar(4000), callIdentifier)
      .input('queue_name', mssql.NVarChar(4000), queueName)
      .input('script_selector_id', mssql.Int, scriptSelectorId)
      .input('mrd_id', mssql.Int, mrdId)
      .input('max_task_limit', mssql.Int, maxTaskLimit)
      .query(query)
      // done
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // set eGain queue to deleted state (does not remove row from DB, for reporting and auditing purposes)
  async delete (arg) {
    // allow queueId to be supplied as single argument or on an object
    let queueId
    if (typeof arg === 'number') {
      queueId = arg
    } else if (typeof arg === 'string') {
      queueId = parseInt(arg)
    } else if (typeof arg === 'object') {
      queueId = arg.queueId
    } else {
      throw Error('queueId is a required input parameter for queue.delete.')
    }
    // delete queue
    const query = queries.delete()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .query(query)
      // done
      return
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async find ({
    queueName,
    departmentName
  }) {
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_name', mssql.NVarChar, queueName)
      .input('department_name', mssql.NVarChar, departmentName)
      .query(query)
      try {
        // has results
        return results.recordsets[0][0] || null
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async addWatcher ({
    queueId,
    instanceId = 0
  }) {
    const query = queries.addWatcher()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('instance_id', mssql.BigInt, instanceId)
      .input('queue_id', mssql.BigInt, queueId)
      .query(query)
      try {
        // has results
        return results.recordsets[0][0] || null
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }


}
