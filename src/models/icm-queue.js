const mssql = require('mssql')
const queries = require('../sql/icm-queue')

module.exports = class {
  constructor (config) {
    this.config = config
  }

  async list () {
    // list all ICM queue associations
    const query = queries.list()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .query(query)
      try {
        // has results
        return results.recordsets[0]
      } catch (e) {
        // no results - return empty array for consistent output
        return []
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async find ({queueId}) {
    // find an ICM queue association for given queueId
    const query = queries.find()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.Int, queueId)
      .query(query)
      try {
        // has results
        return results.recordsets[0][0]
      } catch (e) {
        // no results - return null
        return null
      }
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async create ({
    queueId,
    mrdId,
    scriptSelectorId,
    maxTaskLimit = 5000
  }) {
    // validate input
    if (!queueId || !mrdId || !scriptSelectorId) {
      throw Error('queueId, mrdId, and scriptSelectorId are required parameters for icmQueue.create. They should be provided as properties of a single input object.')
    }
    // list all queues
    const query = queries.create()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.Int, queueId)
      .input('mrd_id', mssql.Int, mrdId)
      .input('script_selector_id', mssql.Int, scriptSelectorId)
      .input('max_task_limit', mssql.Int, maxTaskLimit)
      .query(query)
      // done
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async update ({
    queueId,
    mrdId,
    scriptSelectorId,
    maxTaskLimit = 5000
  }) {
    // validate input
    if (!queueId || !mrdId || !scriptSelectorId) {
      throw Error('queueId, mrdId, and scriptSelectorId are required parameters for icmQueue.create. They should be provided as properties of a single input object.')
    }
    // list all queues
    const query = queries.udpate()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .input('mrd_id', mssql.Int, mrdId)
      .input('script_selector_id', mssql.BigInt, scriptSelectorId)
      .input('max_task_limit', mssql.BigInt, maxTaskLimit)
      .query(query)
      // done
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  // delete ICM queue from database
  async delete (arg) {
    // allow queueId to be supplied as single argument or on an object
    let queueId
    if (typeof arg === 'number') {
      queueId = arg
    } else if (typeof arg === 'string') {
      queueId = parseInt(arg)
    } else if (typeof arg === 'object') {
      queueId = arg.queueId
    } else {
      throw Error('queueId is a required input parameter for icmQueue.delete.')
    }
    // delete queue
    const query = queries.delete()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .query(query)
      // done
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async associateCtiVariable ({
    queueId,
    callVariableName,
    callVariableTag
  }) {
    const query = queries.associateCtiVariable()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .input('call_variable_name', mssql.NVarChar, callVariableName)
      .input('call_variable_tag', mssql.Int, callVariableTag)
      .query(query)
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async removeCtiVariables ({
    queueId
  }) {
    const query = queries.removeCtiVariables()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .query(query)
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async mapPq ({
    queueId,
    pqId
  }) {
    const query = queries.mapPq()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .input('pq_id', mssql.Int, pqId)
      .query(query)
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async unmapPq ({
    queueId
  }) {
    const query = queries.unmapPq()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .query(query)
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async mapSq ({
    queueId,
    sgId
  }) {
    const query = queries.mapSg()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .input('skill_target_id', mssql.Int, sgId)
      .query(query)
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }

  async unmapSq ({
    queueId
  }) {
    const query = queries.unmapSg()
    try {
      const pool = await new mssql.ConnectionPool(this.config).connect()
      const results = await pool.request()
      .input('queue_id', mssql.BigInt, queueId)
      .query(query)
      return results
    } catch (e) {
      throw e
    } finally {
      mssql.close()
    }
  }
}
